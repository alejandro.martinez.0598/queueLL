package queue;

import java.util.Iterator;

import nodo.Nodo;

public class QLinkedList<T> implements Iterable<T>,iQueue<T>{

	private Nodo<T> sent = null;
	private int lenght;

	public QLinkedList() {
		sent = new Nodo<T>();
		sent.setIndex(-1);
		lenght = 10; 
	}
		
	public QLinkedList(T value,int l){
		this();
		Nodo<T> tmp = new Nodo<T>(value);
		tmp.setIndex(0);
		sent.setNext(tmp);
		lenght = l; 
	}
	
	public QLinkedList(int l){
		this();
		lenght = l; 
	}

	@Override
	public void enqueue(T d) throws QueueFullException {
		if(!isFull()){
		Nodo<T> temp = sent;	
		addEndRec(d,temp);
		reIndex();	
		}
		else
			throw new QueueFullException("Full queue");
	}
	private void addEndRec(T d,Nodo<T> n){
		if(n.getNext()== null){
			Nodo<T> ne = new Nodo<T>(d);
			n.setNext(ne);
			return;
		}
		else
			addEndRec(d,n.getNext());
	}

	@Override
	public T deQueue() throws QueueEmptyException {
		 if(!isEmpty()){
			 T temp = getFirst().getValue();
			 removeFirst();
			 return temp;
		 }
		 throw new QueueEmptyException("Empty queue");
	}

	@Override
	public boolean isEmpty() {
		if(sent.getNext() != null)
			return false;
		
			return true;
	}

	@Override
	public boolean isFull() {
		
		return (size()>= lenght);
	}

	@Override
	public T front() throws QueueEmptyException {
		 if(!isEmpty()){
			 T temp = getFirst().getValue();
			 return temp;
		 }
		 throw new QueueEmptyException("Empty queue");
	}

	@Override
	public T search(T value) throws QueueEmptyException {
		 if(!isEmpty()){
			 T temp = SearchRec(value).getValue();
			 return temp;
		 }
		 throw new QueueEmptyException("Empty queue");
	}

	@Override
	public void clear() {
		Nodo<T> temp = sent;
		 while (temp.getNext() != null){
			 Nodo<T> temp2 = temp.getNext();
			 temp.setNext(null);
			 temp = temp2;
		 }
		 System.gc();	
	}

	@Override
	public boolean frontOf(T value, int priority) throws QueueFullException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size(){
		reIndex();
		return (int) getLast().getIndex();
	 }

	
	public Nodo<T> SearchRec(T temp){
	      return SearchRec(sent,temp);
	}
   private Nodo<T> SearchRec(Nodo<T> se,T comp){
			if (se.getNext()==null){
				return null;
			}
			if(se.getNext().getValue().equals(comp)){
					return se.getNext();
				}
			return 	SearchRec(se.getNext(),comp);
					
	}
   
   public Nodo<T> remove(T bus){
		Nodo<T> temp = sent;
		
			while (temp.getNext()!=null){
				if(temp.getNext().getValue().equals(bus)){
					Nodo<T> fin =temp.getNext();
					temp.setNext(temp.getNext().getNext());
					//System.out.println(fin.getValue());
					return fin;
				}
				temp = temp.getNext();
			}
				return null;	
	}
   public void removeFirst(){
		remove( getFirst().getValue());
	 }
	
	///
  
        
     
   
	 
    
     
     
     
    
       
	
	
	
	

	
	
	// imprime la lista de manera recursiva 
	public void print(){
		Nodo<T> temp = sent;
		while(temp.getNext()!=null){
			temp = temp.getNext();
			System.out.println(temp.getValue());
		}
	}
	public Nodo<T> print(Nodo<T> n){
		System.out.println(n.getValue());
		if(n.getNext() != null)
		    return print(n.getNext());
		else
			return null;
	}
	
	public Nodo<T> getByIndex(int i){
		 Nodo<T> temp = sent;
		 int n=0;
		 while ( n != i ) {
			 if(temp.getNext() != null)
			      temp= temp.getNext();
			n++;
		 }
		 return temp;
	}
	
	// imprime la lista de manera recursiva
	public void printRec(){
		print(sent.getNext());
	}
	
	 public Nodo<T>  getFirst(){
		 return sent.getNext();
	 }
	 public Nodo<T> getLast(){
		 Nodo<T> temp = sent;	
			while(temp.getNext()!=null)
				temp = temp.getNext();
			return temp;
	 }
	 
	 public void removeLast(){
		 remove( getLast().getValue());
	 }
	 
	 public boolean replace (T val , T nu){
		 Nodo<T> temp  = SearchRec(val);
		 if (temp != null){
			 temp.setValue(nu);
		 return true;
		 }
	
		 return false ;
	 }

	 public int indexOf (T value){
		
		 Nodo<T> temp = SearchRec(value);
			 if(temp != null)
			      return (int) temp.getIndex();
			 else return -1;
			
	 }
	 
	 
	 
	 public boolean removeAfter(T value){
		 Nodo <T> tmp = SearchRec(value);
		 if (tmp.getNext() != null){
			 tmp.setNext(tmp.getNext().getNext());
			 reIndex();
			 return true;
		 }
		 return false;
		 }

	 public boolean removeBefore(T value){
		 Nodo <T> tmp = sent;
		 if (tmp.getNext().getValue().equals(value)) return false;
		 while (tmp.getNext().getNext() != null){
			 if (tmp.getNext().getNext().getValue().equals(value)){
				 tmp.setNext(tmp.getNext().getNext());
				 return true;
			 }
			 tmp = tmp.getNext();			 
		 }
		 return false;
	 }
	 
	 @Override
		public Iterator<T> iterator() {
			
			return new Iterator<T>(){
				int i = 0;
			
				@Override
				public boolean hasNext() {
					
						return getByIndex(i).getNext()!=null ? true : false ;
				
				}

				@Override
				public T next() {
					
						return (T) getByIndex(++i).getValue();
					
				}
			
				
			};
	 }
	
	
	public void reIndex(){
		Nodo<T> temp = sent;
		int cont = 1;
		while(temp.getNext()!=null){
		temp = temp.getNext();
		temp.setIndex(cont++);
		}
	}
	
	}
