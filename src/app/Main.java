package app;

import queue.QLinkedList;
import queue.Queue;

public class Main {

    public static void main(String[] args) {
	// write your code here
    	QLinkedList<String> names = new QLinkedList<String>(5);
    	try {
            names.enqueue("Joselito");
            names.enqueue("Ana");
            names.enqueue("Ricardo");
            names.enqueue("Aaron");
            names.enqueue("Milton");
            names.deQueue();
            names.enqueue("EvilIn");
            //names.enqueue("6to");
            System.out.println("Front: " + names.front());
            System.out.println("Size: " + names.size());
            System.out.println("Search: " + names.search("Ana"));
            System.out.println("Print todo:");
            names.print();
            
            
            for (String string : names) {
				System.out.println("Iterator: " + string);
			}

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
